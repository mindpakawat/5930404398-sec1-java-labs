package tongchanda.pakawat.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import tongchanda.pakawat.lab4.Gender;

public class TerminalPatient extends PatientV2{

	public TerminalPatient(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
		
	}

	private String terminalDisease ;
	private LocalDate firstDiagnosed;
	public TerminalPatient(String name, String birthdate, Gender gender, double weight, int height,
			String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}
	
	

	
	public String getTerminalDisease() {
		return terminalDisease;
	}
	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}
	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}
	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}




	@Override
	public String toString(){
		return "TerminalPatient [ "+terminalDisease+ ", " +firstDiagnosed+"], Patient ["+ getName() +", "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm.]";
		
	}
	public void patientReport(){
		System.out.println("You have terminal illness");
		
	}
	
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
}
