package tongchanda.pakawat.lab2;

//This program will be input and out information of patient
//Pakawat Tongchanda 593040439-8 sec1

public class PatientV2 {

	public static void main(String[] args) {

		if (args.length != 4)
			System.err.println("Patient <name> <gender> <weight> <height>");

		else {
			
			float weight = Float.parseFloat(args[2]);
			int height = Integer.parseInt(args[3]);

			System.out.println("This patient name is " + args[0]);

			if (args[1].equalsIgnoreCase("Male") && args[1].equalsIgnoreCase("FeMale") && weight > 0 && height > 0) {

				if (args[1].equalsIgnoreCase("Male")) {
					System.out.println("His weight is " + args[2] + " kg. and heigh is " + args[3] + " cm.");
				} else if (args[1].equalsIgnoreCase("Female")) {
					System.out.println("Her weight is " + args[2] + " kg. and heigh is " + args[3] + " cm.");
				}
			} 
			else {
				System.out.println("Please enter gender as only Male of Female");

				if (weight <= 0)
					System.out.println("Weight must be non-negative");
				if (height <= 0)
					System.out.println("Height must be non-negative");

			}
		}
	}
}
