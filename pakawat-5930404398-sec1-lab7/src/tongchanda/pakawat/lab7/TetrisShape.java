package tongchanda.pakawat.lab7;

import java.awt.*;

public class TetrisShape {

	
	Tetrimino shape;
	Color color;
	
	
	protected int coordinateX [];
	protected int coordinateY[] ;
	
	

	public Tetrimino getShape(){
		
		return shape;
	}
	
	public void setShape(Tetrimino shape){
		
		this.shape = shape;
	}
	
	public Color getColor(){
		return color;
	}
	
	public TetrisShape(Tetrimino realShape){
		this.shape = realShape;
		setShapeColor(realShape);
		setShapeLocate(realShape);
		
	}
	
	private void setShapeColor(Tetrimino shape){
		
		switch(shape) {
		case Z_SHAPE:
			color = new Color(255,0,0);
			break;
		case S_SHAPE:
			color = new Color(0,255,0);
			break;
		case I_SHAPE:
			color = new Color(0,255,255);
			break;
		case T_SHAPE:
			color = new Color(170,0,255);
			break;
		case O_SHAPE:
			color = new Color(255,255,0);
			break;
		case L_SHAPE:
			color = new Color(255,165,0);
			break;
		case J_SHAPE:
			color = new Color(0,0,255);
			break;
			
		}
	}
	
	private void setShapeLocate(Tetrimino shape){
		switch(shape){
		case Z_SHAPE:
			coordinateX = new int[] {1,1,0,0};
			coordinateY = new int[] {0,1,1,2};
			break;
		case S_SHAPE:
			coordinateX = new int[] {0,0,1,1};
			coordinateY = new int[] {0,1,1,2};
			break;
		case I_SHAPE:
			coordinateX = new int[] {0,0,0,0};
			coordinateY = new int[] {0,1,2,3};
			break;
		case T_SHAPE:
			coordinateX = new int[] {0,1,2,1};
			coordinateY = new int[] {0,0,0,1};
			break;
		case O_SHAPE:
			coordinateX = new int[] {0,1,0,1};
			coordinateY = new int[] {0,0,1,1};
			break;
		case L_SHAPE:
			coordinateX = new int[] {0,1,1,1};
			coordinateY = new int[] {0,0,1,2};
			break;
		case J_SHAPE:
			coordinateX = new int[] {1,0,0,0};
			coordinateY = new int[] {0,0,1,2};
			break; 
		}
		
	}
}
