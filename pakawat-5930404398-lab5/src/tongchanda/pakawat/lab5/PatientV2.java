package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.*;

public class PatientV2 extends Patient{

	public PatientV2(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
		
	}
	
	public void patientReport(){
		
		System.out.println("You need medical attention");
	}
	
}
