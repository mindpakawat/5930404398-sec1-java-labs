package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.Gender;

public class VIPPatientV2 extends PatientV3 implements HasInsurance, HasPet{
	
	private double totalDonation;
	private String passPhrase;

	
	private static String encrypt(String message) {
		char[] arrayEncrypt = message.toCharArray();
		for (int i = 0; i < message.length(); i++) {
			arrayEncrypt[i] = (char) ((arrayEncrypt[i] - 90) % 26 + 97);
		}
		String encryptString = new String(arrayEncrypt);
		return encryptString;
	}

	private static String decrypt(String deMessage) {
		char[] arrayDecrypt = deMessage.toCharArray();
		for (int i = 0; i < deMessage.length(); i++) {
			arrayDecrypt[i] = (char) ((arrayDecrypt[i] - 104) % 26 + 97);
			if (arrayDecrypt[i] < 97)
				arrayDecrypt[i] += 26;
		}
		String decryptString = new String(arrayDecrypt);
		return decryptString;
	}
	
	public String getVIPName(String passPhrase) {
		if (this.passPhrase.equals(passPhrase)) {
			return decrypt(super.getName());
		} else {
			return "Wrong passphrase, please try again";
		}
	}
	public double getTotalDonation() {
		return totalDonation;
	}
	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}
	public String getPassPhrase() {
		return passPhrase;
	}
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}
	
	public void donate(double donate){
		this.totalDonation = this.totalDonation+donate;
		
		
	}
	
	
	
	
	public VIPPatientV2(String name, String birthdate, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdate, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
		
	}

	@Override
	public void feedPet() {
		System.out.println("feed the pet.");
		
	}

	@Override
	public void playWithPet() {
		System.out.println("pay with pet.");
		
	}

	@Override
	public void pay() {
		
		
	}

	@Override
	public void pay(double amount) {
		
		
	}

}
