package tongchanda.pakawat.lab6;

// This program is develop from PatientFormV2 and add menu bar 

// Author Pakawat Tongchanda

// 02/03/2017

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

public class PatientFormV3 extends PatientFormV2{

	
	private static final long serialVersionUID = 1L;
	protected JLabel patientTypeText = new JLabel();
	protected JPanel patientTypeField = new JPanel();
	protected JComboBox patientTypeBox = new JComboBox();
	protected JMenuBar patientMenuBar = new JMenuBar();
	protected JMenu patientFile = new JMenu();
	protected JMenu patientConfig = new JMenu();
	protected JMenuItem patientFile_New = new JMenuItem();
	protected JMenuItem patientFile_Open = new JMenuItem();
	protected JMenuItem patientFile_Save = new JMenuItem();
	protected JMenuItem patientFile_Exit = new JMenuItem();
	protected JMenuItem patientConfig_Color = new JMenuItem();
	protected JMenuItem patientConfig_Size = new JMenuItem();

	public PatientFormV3(String string) {
		super(string);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	public static void createAndShowGUI(){
		
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
		
	}
	
	protected void setFrameFeatures(){
		super.setFrameFeatures();
		
	}
	
	protected void addComponents(){
		//create box and set location
		super.addComponents();
		patientTypeText = new JLabel("Type:");
		patientTypeBox.addItem("Inpatient");
		patientTypeBox.addItem("Outpatient");
		patientTypeBox.setPreferredSize(new Dimension(202, 20));
		patientTypeBox.setEditable(true);
		patientTypeBox.setSelectedItem("Outpatient");
		patientTypeField = new JPanel(new BorderLayout());
		patientTypeField.add(patientTypeText, BorderLayout.WEST);
		patientTypeField.add(patientTypeBox,BorderLayout.EAST);
		centreOutput.add(patientTypeField,BorderLayout.SOUTH);
		//create and set manu bar
		patientFile = new JMenu("File");
		patientConfig = new JMenu("Config");
		patientFile_New =  new JMenuItem("New");
		patientFile_Open = new JMenuItem("Open");
		patientFile_Save = new JMenuItem("Save");
		patientFile_Exit = new JMenuItem("Exit");
		patientConfig_Color = new JMenuItem("Color");
		patientConfig_Size = new JMenuItem("Size");
		patientFile.add(patientFile_New);
		patientFile.add(patientFile_Open);
		patientFile.add(patientFile_Save);
		patientFile.add(patientFile_Exit);
		patientConfig.add(patientConfig_Color);
		patientConfig.add(patientConfig_Size);
		patientMenuBar.add(patientFile);
		patientMenuBar.add(patientConfig);
		setJMenuBar(patientMenuBar);
		
	}
}
