package tongchanda.pakawat.lab8;


import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;


public class PatientFormV6 extends PatientFormV5 {

	
	private static final long serialVersionUID = 1L;
	protected JMenuItem newMenu = new JMenuItem("New");
	protected JMenuItem openMenu = new JMenuItem("Open", new ImageIcon("image/openIcon.png"));
	protected JMenuItem saveMenu = new JMenuItem("Save", new ImageIcon("image/saveIcon.png"));
	protected JMenuItem exitMenu = new JMenuItem("Exit", new ImageIcon("image/quitIcon.png"));
	protected JPanel imagePanel = new JPanel(new BorderLayout());	
	protected JLabel imageLabel = new JLabel();
	ImageIcon pictureManee = new ImageIcon("image/manee.jpg");

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public JMenuBar createMenuBar(){
		super.createMenus();
		fileMenu.removeAll();
		fileMenu.add(newMenu);
		fileMenu.add(openMenu);
		fileMenu.add(saveMenu);
		fileMenu.add(exitMenu);
		return menuBar;
	}
	
	public void addComponents() {
		super.addComponents();
		imageLabel.setIcon(pictureManee);
		imageLabel.setHorizontalAlignment(JLabel.CENTER);
		imageLabel.setBorder(BorderFactory.createEmptyBorder(30, 40, 30, 40));
		imagePanel.add(imageLabel, BorderLayout.CENTER);
		overallPanel.add(imagePanel, BorderLayout.NORTH);
		
		
	}
	
	

	public PatientFormV6(String string) {
		super(string);
	}
	
	public static void createAndShowGUI() {
		PatientFormV6 patientFormV6 = new PatientFormV6 ("Patient Form V6");
		patientFormV6.setJMenuBar(patientFormV6.createMenuBar());
		patientFormV6.addComponents();
		patientFormV6.setFrameFeatures();
		patientFormV6.addListeners();
		
	}
	protected void actionPerform(ActionEvent arg0) {
		
		
	}
	
	

}
