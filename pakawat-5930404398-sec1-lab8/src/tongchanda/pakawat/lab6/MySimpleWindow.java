package tongchanda.pakawat.lab6;




import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6029172243670358973L;
	
	protected JButton cancelButton, okButton;
	protected JPanel buttonsPanel;
	
	public MySimpleWindow(String title) {
		super(title);
	}
	
	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		buttonsPanel = new JPanel();
		buttonsPanel.add(cancelButton);
		buttonsPanel.add(okButton);
		this.setContentPane(buttonsPanel);
	}
	
	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	 
	public static void createAndShowGUI(){
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}