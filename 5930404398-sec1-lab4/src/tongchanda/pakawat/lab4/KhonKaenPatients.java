package tongchanda.pakawat.lab4;

// This program will be print information of patient

// author Pakawat Tongchanda

// 31/01/2017


import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class KhonKaenPatients {

	public static void main(String[] args) {
		Patient mana = new Patient("Mana", "20.01.1990", Gender.MALE, 58.7, 160);
		Patient manee = new Patient("Manee","12.02.1995", Gender.FEMALE, 52.7, 150);
		mana.setWeight(60.7);
		LocalDate today = LocalDate.now();
		Long yearsDelta = mana.getBirthdate().until(today, ChronoUnit.YEARS);
		System.out.println("mana is " + yearsDelta + " years old");
		System.out.println("Manee's height is " + manee.getHeight() + " cm.");
		System.out.println(mana);
		System.out.println(manee);
		
	}

}
