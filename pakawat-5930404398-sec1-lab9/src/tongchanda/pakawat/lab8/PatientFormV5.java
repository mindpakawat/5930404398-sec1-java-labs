package tongchanda.pakawat.lab8;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class PatientFormV5 extends PatientFormV4{

	
	private static final long serialVersionUID = 1L;
	protected JMenu colorMenu = new JMenu("Color");
	protected JMenu sizeMenu = new JMenu("Size");
	protected JMenuItem blueColor = new JMenuItem("Blue");	
	protected JMenuItem greenColor = new JMenuItem("Green");
	protected JMenuItem redColor = new JMenuItem("Red");
	protected JMenuItem customColor = new JMenuItem("Custom...");
	protected JMenuItem sixteenSize = new JMenuItem("16");
	protected JMenuItem twentySize = new JMenuItem("20");
	protected JMenuItem twentyFourSize = new JMenuItem("24");
	protected JMenuItem customSize = new JMenuItem("Custom...");

		public JMenuBar createMenus(){
		super.addMenus();
		configMenu.removeAll();
		configMenu.add(colorMenu);
		configMenu.add(sizeMenu);
		colorMenu.add(blueColor);
		colorMenu.add(greenColor);
		colorMenu.add(redColor);
		colorMenu.add(customColor);
		sizeMenu.add(sixteenSize);
		sizeMenu.add(twentySize);
		sizeMenu.add(twentyFourSize);
		sizeMenu.add(customSize);
		return menuBar;
	}
	
	public void addListeners() {
		super.addListeners();
		blueColor.addActionListener(this);
		greenColor.addActionListener(this);
		redColor.addActionListener(this);
		sixteenSize.addActionListener(this);
		twentySize.addActionListener(this);
		twentyFourSize.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object event = e.getSource();
		if (event == blueColor){
			nameTxtField.setForeground(Color.BLUE);
			dateTxtField.setForeground(Color.BLUE);
			weightTxtField.setForeground(Color.BLUE);
			heightTxtField.setForeground(Color.BLUE);
			addrTxtArea.setForeground(Color.BLUE);
		} else if (event == greenColor){
			nameTxtField.setForeground(Color.GREEN);
			dateTxtField.setForeground(Color.GREEN);
			weightTxtField.setForeground(Color.GREEN);
			heightTxtField.setForeground(Color.GREEN);
			addrTxtArea.setForeground(Color.GREEN);
		} else if (event == redColor){
			nameTxtField.setForeground(Color.RED);
			dateTxtField.setForeground(Color.RED);
			weightTxtField.setForeground(Color.RED);
			heightTxtField.setForeground(Color.RED);
			addrTxtArea.setForeground(Color.RED);
		} 
		
		Font font16 = new Font("SansSerif", Font.BOLD, 16);
		Font font20 = new Font("SansSerif", Font.BOLD, 20);
		Font font24 = new Font("SansSerif", Font.BOLD, 24);
		
		if (event == sixteenSize){
			nameTxtField.setFont(font16);
			dateTxtField.setFont(font16);;
			weightTxtField.setFont(font16);
			heightTxtField.setFont(font16);
			addrTxtArea.setFont(font16);
		} else if (event == twentySize){
			nameTxtField.setFont(font20);
			dateTxtField.setFont(font20);;
			weightTxtField.setFont(font20);
			heightTxtField.setFont(font20);
			addrTxtArea.setFont(font20);
		} else if (event == twentyFourSize){
			nameTxtField.setFont(font24);
			dateTxtField.setFont(font24);;
			weightTxtField.setFont(font24);
			heightTxtField.setFont(font24);
			addrTxtArea.setFont(font24);
		}

	}

	public PatientFormV5(String string) {
		super(string);
	}
	
	public static void createAndShowGUI() {
		PatientFormV5 patientFormV5 = new PatientFormV5 ("Patient Form V5");
		patientFormV5.setJMenuBar(patientFormV5.createMenus());
		patientFormV5.addComponents();
		patientFormV5.setFrameFeatures();
		patientFormV5.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
