package tongchanda.pakawat.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class InPatient extends Patient{

	public InPatient(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
		
	}

	private LocalDate admitDate;
	private LocalDate dischargeDate;
	public InPatient(String name, String birthdate, Gender gender, double weight, int height, String admitDate,
			String dischargeDate) {
		super(name, birthdate, gender, weight, height);
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);
	}
	

	public LocalDate getAdmitDate() {
		return admitDate;
	}


	public void setAdmitDate(String admitDate) {
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);;
	}


	public LocalDate getDischargeDate() {
		return dischargeDate;
	}


	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);;
	}

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
	public String toString(){
		return "InPatient ["+ getName() +", birthdate = "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm. "+"admitDate = "+admitDate+", dischargeDate = "+dischargeDate+"]";
	}
}
