package tongchanda.pakawat.lab2;

//This program will be change bank note
//pakawat tongchanda 593040439-8 sec1

public class MoneyChanger {

	public static void main(String[] args) {
		int money = Integer.parseInt(args[0]);
		int bankChange = 1000 - money;
		int change;

		// input money
		System.out.println("You give 1000 for the cost as " + money + " Bath.");

		// change money and output change
		if (money == 1000) {
			System.out.println("No change.");
		}

		else if (money < 1000 ) {

			System.out.println("You will receive exchange as " + bankChange + " Bath.");

			if (bankChange >= 500) {
				bankChange %= 500;
				System.out.print("1 of 500 bank note; ");
			}

			if (bankChange >= 100) {
				change = bankChange / 100;
				bankChange %= 100;
				System.out.print(change + " of 100 bank note; ");
			}

			if (bankChange >= 50) {
				change = bankChange / 50;
				bankChange %= 50;
				System.out.print(change + " of 50 bank note; ");
			}

			if (bankChange >= 20) {
				change = bankChange / 20;
				bankChange %= 20;
				System.out.print(change + " of 20 bank note; ");
			}

		}

	}

}
