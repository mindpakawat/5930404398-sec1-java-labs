package tongchanda.pakawat.lab6;

// This program is develop from PatientFormV1 and add gender radio button

// Autor Pakawat Tongchanda

// 02/03/2017

import java.awt.*;
import javax.swing.*;

public class PatientFormV2 extends PatientFormV1{

	
	private static final long serialVersionUID = 1L;
	protected JLabel genderText = new JLabel();
	protected JLabel addressText = new JLabel();
	protected JRadioButton genderMale = new JRadioButton();
	protected JRadioButton genderFeMale = new JRadioButton();
	protected JPanel genderPanel = new JPanel();
	protected JPanel genderField = new JPanel();
	protected JPanel addressTextPanel = new JPanel();
	protected JPanel centreOutput = new JPanel();
	protected ButtonGroup groupOfButton = new ButtonGroup();
	protected JTextArea addressArea = new JTextArea();
	protected JScrollPane addressScoll = new JScrollPane();
	
	 public static void createAndShowGUI(){
			PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
			patientForm2.addComponents();
			patientForm2.setFrameFeatures();
		}

	public PatientFormV2(String string) {
		super(string);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});


	}
	
	protected void addComponents(){
		//create and set gender radio button
		super.addComponents();
		genderText = new JLabel("Gender:");
		genderMale = new JRadioButton("Male");
		genderFeMale = new JRadioButton("Female");
		groupOfButton.add(genderMale);
		groupOfButton.add(genderFeMale);
		genderPanel = new JPanel(new FlowLayout());
		genderPanel.add(genderMale);
		genderPanel.add(genderFeMale);
		genderField.setLayout(new GridLayout(1,1));
		genderField.add(genderText);
		genderField.add(genderPanel);
		//create and set address box
		addressText = new JLabel("Address:");
		addressArea = new JTextArea(2,35);
		addressScoll = new JScrollPane(addressArea);
		addressArea.setLineWrap(true);
		addressArea.setWrapStyleWord(true);
		addressArea.setText("Department of Computer Engineering, Faculty of Engineering, Khon Kaen University, Mittraparp Rd., T. Naimuang, A. Muang, Khon Kaen, Thailand, 40002");
		addressTextPanel.setLayout(new GridLayout(2,0));
		addressTextPanel.add(addressText);
		addressTextPanel.add(addressScoll);
		//Output display
		centreOutput = new JPanel(new BorderLayout());
		centreOutput.add(genderField,BorderLayout.NORTH);
		centreOutput.add(addressTextPanel, BorderLayout.CENTER);
		display.add(centreOutput, BorderLayout.CENTER);
		
	
		
	}

}
