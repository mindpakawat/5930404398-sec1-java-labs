package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.Gender;
import tongchanda.pakawat.lab4.InPatient;
import tongchanda.pakawat.lab4.OutPatient;
import tongchanda.pakawat.lab4.Patient;


public class KhonKaenPatientsV3 {

	public static void main(String[] args) {

		Patient manee = new InPatient("Manee", "01.12.1980", Gender.FEMALE, 60, 150, "20.01.2017", "29.01.2017");
		Patient mana = new OutPatient("Mana", "22.04.1981", Gender.MALE, 70, 160, "23.01.2017");
		Patient chujai = new Patient("Chujai", "03.03.1980", Gender.FEMALE, 41.5, 175);
		InPatient piti = new InPatient("Piti", "05.05.1980", Gender.MALE, 65, 165, "11.01.2017", "17.01.2017");
		System.out.println(manee);
		System.out.println(mana);
		System.out.println(chujai);
		System.out.println(piti);
		InPatient manee_new = (InPatient)manee;
		
		manee_new.setDischargeDate("31.01.2017");
		System.out.println("The new discharge date for " + manee_new.getName() + " is " + manee_new.getDischargeDate() + ".");
		OutPatient mana_new = (OutPatient)mana;
	
		mana_new.setVisitDate("14.02.2017");
		System.out.println("The new visit date for " + mana_new.getName() + " is " + mana_new.getVisitDate() + ".");

		piti.setAdmitDate("15.01.2017");
		System.out.println("The new admit date for " + piti.getName() + " is " + piti.getDischargeDate() + ".");

	}
}
