package tongchanda.pakawat.lab5;

public interface UnderLegalAge {
	
	void askPermission();
	void askPermission(String legalGuardianName);

}
