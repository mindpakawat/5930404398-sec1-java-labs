package tongchanda.pakawat.lab8;

import java.awt.event.*;
import javax.swing.*;
import tongchanda.pakawat.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener,ItemListener{

	
	private static final long serialVersionUID = 1L;
	
	
	
	public PatientFormV4(String string) {
		super(string);
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI(){
		PatientFormV4 PV4 = new PatientFormV4("Patient Form V4");
		PV4.addComponents();
		PV4.addMenus();
		PV4.setFrameFeatures();
		PV4.addListeners();
		
		
	}
	
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		typeList.addActionListener(this);
		maleRadioB.addItemListener(this);
		femaleRadioB.addItemListener(this);
		
		
	}
	public void addComponents(){
		super.addComponents();
	}
	
	public void setFrameFeatures(){
		super.setFrameFeatures();
		
	}
	
	protected void cancleEmty(){
		String emt = "";
		nameTxtField.setText(emt);
		dateTxtField.setText(emt);
		weightTxtField.setText(emt);
		heightTxtField.setText(emt);
		addrTxtArea.setText(emt);
		genderGrpB.clearSelection();
	}
	
	@Override
	public void itemStateChanged(ItemEvent arg0) {
		Object item = arg0.getItem();
		if(((JRadioButton)item).isSelected() ){
			if(item == maleRadioB){
				JOptionPane male = new JOptionPane("Your gender type is now change to male");
				JDialog gender = male.createDialog("Gender info");
				gender.setLocation(getX()+5, getY()+325);
				gender.setVisible(true);
			}
			else if(item == femaleRadioB){
				JOptionPane female = new JOptionPane("Your gender type is now change to female");
				JDialog gender = female.createDialog("Gender info");
				gender.setLocation(getX()+5, getY()+325);
				gender.setVisible(true);
			}
		
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object src = arg0.getSource();
		String information = "Name = "+nameTxtField.getText()+" Birthdate = "+dateTxtField.getText()+" Weight = "+weightTxtField.getText()+" Height = "+heightTxtField.getText()+
				 "\nGender = "+ (maleRadioB.isSelected() ? "Male" : (femaleRadioB.isSelected() ? "Female": ""))+
				 "\nAddress = "+addrTxtArea.getText()+
				 "\nType = "+ (typeList.getSelectedItem());
		if(src == okButton){
			JOptionPane.showMessageDialog(this , information);
		}
		else if (src == cancelButton){
			cancleEmty();
		}
		else if (src == typeList){
			if(typeList.getSelectedItem() == "Outpatient")
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Outpatient");
			
		}
			if(typeList.getSelectedItem() == "Inpatient"){
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Inpatient");
		}
	}

}
