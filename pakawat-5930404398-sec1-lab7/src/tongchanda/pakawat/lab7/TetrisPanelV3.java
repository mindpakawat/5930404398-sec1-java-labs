package tongchanda.pakawat.lab7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class TetrisPanelV3 extends TetrisPanelV2 {

		private static final long serialVersionUID = 1L;

		private ArrayList<Rectangle2D.Double> notMovingRect;
		private int numNotMovingRect;
		protected ArrayList<Color> notMovingRectColor;
		private Rectangle2D.Double movingRect;
		protected final static int RECT_WIDTH = 60;
		protected final static int RECT_HEIGHT = 40;
		protected int coordinateX, coordinateY;
		protected int randomRed, randomGreen, randomBlue;

		public TetrisPanelV3() {
			super();
			notMovingRect = new ArrayList<>();
			notMovingRectColor = new ArrayList<>();
			coordinateX = (WIDTH - RECT_WIDTH) / 2;
			coordinateY = 0;
			numNotMovingRect = 0;
			randomRed = (int) (Math.random() * 256);
			randomGreen = (int) (Math.random() * 256);
			randomBlue = (int) (Math.random() * 256);
		}

		@Override
		public void run() {
			while (true) {
				if ((coordinateY + SPEED) + RECT_HEIGHT >= HEIGHT+10) {
					notMovingRect.add(movingRect);
					numNotMovingRect++;
					coordinateX = (int) (Math.random() * (WIDTH - RECT_WIDTH) + 1);
					coordinateY = 0;
					notMovingRectColor.add(new Color(randomRed, randomGreen, randomBlue));
					randomRed = (int) (Math.random() * 256);
					randomGreen = (int) (Math.random() * 256);
					randomBlue = (int) (Math.random() * 256);
				} else {
					coordinateY += SPEED;
				}
				repaint();

				
				try {
					Thread.sleep(10);
				} catch (InterruptedException ex) {
				}
			}
		}

		public void paint(Graphics g) {
			super.paintComponent(g);
			setBackground(Color.WHITE);
			Graphics2D draw = (Graphics2D) g;

			for (int n = 0; n < numNotMovingRect; n++)
				if (numNotMovingRect > 0) {
					draw.setColor(notMovingRectColor.get(n));
					draw.fill(notMovingRect.get(n));
					draw.setColor(Color.DARK_GRAY);
					draw.draw(notMovingRect.get(n));
				}

			movingRect = new Rectangle2D.Double(coordinateX, coordinateY, RECT_WIDTH, RECT_HEIGHT);
			draw.setColor(new Color(randomRed, randomGreen, randomBlue));
			draw.fill(movingRect);
			draw.setColor(Color.DARK_GRAY);
			draw.draw(movingRect);
		}

}
