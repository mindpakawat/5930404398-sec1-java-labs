package tongchanda.pakawat.lab7;

import java.awt.*;
import javax.swing.*;

public class Tetris extends JFrame{

	
	private static final long serialVersionUID = 1L;

	public Tetris(String string) {
		super(string);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}
	
	protected static void createAndShowGUI(){
		
		Tetris tetris = new Tetris("Coe Tetris Game");
		tetris.addComponents();
		tetris.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
		
	}

	protected void addComponents() {
		TetrisPanel winTertris = new TetrisPanel();
		setContentPane(winTertris);
	}
	
}
