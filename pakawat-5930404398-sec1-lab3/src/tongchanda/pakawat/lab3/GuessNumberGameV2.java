package tongchanda.pakawat.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {

	static int i;
	static int answer;

	public static void main(String[] args) {
		int numGuess = 0, min = 0, max = 0;
		int guess;
		String again;

		Scanner ScGuess = new Scanner(System.in);
		do {
			System.out.print("Enter min and max of random number: ");
			min = ScGuess.nextInt();
			max = ScGuess.nextInt();
			System.out.print("Enter the possible guess : ");
			numGuess = ScGuess.nextInt();

			answer = min + (int) (Math.random() * ((max - min) + 1));

			for (i = numGuess; i > 0; i--) {

				System.out.println("Number of remaining guess : " + i);
				System.out.print("Enter guess: ");
				guess = ScGuess.nextInt();

				if (guess > max || guess < min) {
					System.out.println("Number must be in " + min + " and " + max);
					i++;
				} 
				else {
					if (guess < answer)
						System.out.println("Higher!");
					if (guess > answer)
						System.out.println("Lower!");
					if (guess == answer) {
						System.out.println("Correct!");
						return;
					}
				}
			}
			System.out.println("Your ran out of guesses. The number was " + answer);

			System.out.print("Want to play again? ( Y or y ) : ");
			again = ScGuess.next();

		} while (again.equalsIgnoreCase("y"));

	}
}
