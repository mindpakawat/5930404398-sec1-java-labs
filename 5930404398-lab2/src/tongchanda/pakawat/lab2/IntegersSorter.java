package tongchanda.pakawat.lab2;

// This program will be sorter integer
// Pakawat Tongchanda 593040439-8 sec1

import java.util.Arrays;

public class IntegersSorter 
{

	public static void main(String[] args) 
	{
		int intCount = Integer.parseInt(args[0]);
		int[] intArrays = new int[intCount];

		// input integer to array
		for (int i = 0; i < intCount; i++) {
			intArrays[i] = Integer.parseInt(args[i + 1]);
		}

		// output integer before sorter
		System.out.println("Before sorting:" + Arrays.toString(intArrays));

		// sorter integer
		Arrays.sort(intArrays);

		// output integer after sorter
		System.out.println("After sorting:" + Arrays.toString(intArrays));

	}

}
