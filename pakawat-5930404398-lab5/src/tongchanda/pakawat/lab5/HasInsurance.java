package tongchanda.pakawat.lab5;

public interface HasInsurance {
	
	void pay();
	void pay(double amount);

}
