package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.Gender;

public class VIPPatient extends PatientV2{

	

	private double totalDonation;
	private String passPhrase;

	
	private static String encrypt(String message) {
		char[] arrayEncrypt = message.toCharArray();
		for (int i = 0; i < message.length(); i++) {
			arrayEncrypt[i] = (char) ((arrayEncrypt[i] - 90) % 26 + 97);
		}
		String encryptString = new String(arrayEncrypt);
		return encryptString;
	}

	private static String decrypt(String deMessage) {
		char[] arrayDecrypt = deMessage.toCharArray();
		for (int i = 0; i < deMessage.length(); i++) {
			arrayDecrypt[i] = (char) ((arrayDecrypt[i] - 104) % 26 + 97);
			if (arrayDecrypt[i] < 97)
				arrayDecrypt[i] += 26;
		}
		String decryptString = new String(arrayDecrypt);
		return decryptString;
	}
	
	public String getVIPName(String passPhrase) {
		if (this.passPhrase.equals(passPhrase)) {
			return decrypt(super.getName());
		} else {
			return "Wrong passphrase, please try again";
		}
	}
	public double getTotalDonation() {
		return totalDonation;
	}
	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}
	public String getPassPhrase() {
		return passPhrase;
	}
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}
	
	public void donate(double donate){
		this.totalDonation = this.totalDonation+donate;
		
		
	}
	
	@Override
	public String toString(){
		return "VIPPatient [ "+totalDonation+ ", Patient ["+ getName() +", "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm.]";
	}
	
	public void patientReport(){
		System.out.println("You record is private");
		
	
	}
	
	
	public VIPPatient(String name, String birthdate, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdate, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
		
	}
}
