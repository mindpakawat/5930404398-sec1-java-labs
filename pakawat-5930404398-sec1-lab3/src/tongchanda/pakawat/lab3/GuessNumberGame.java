package tongchanda.pakawat.lab3;

import java.util.Scanner;

public class GuessNumberGame {

	static int remainingGuess = 7;
	static int max = 100, min = 0;
	static int finalAns = min + (int) (Math.random() * ((max - min) + 1));

	public static void main(String[] args) {

		Scanner ScGuess = new Scanner(System.in);

		for (int i = remainingGuess; i > 0; i--) {
			System.out.println("Number of remaining is: " + i);
			System.out.print("Enter a guess: ");
			int guess = ScGuess.nextInt();

			if (guess == finalAns){
				System.out.println("Correct!");
				return;}
			
			else if (guess < finalAns)
				System.out.println("Higher!");
			
			else if (guess > finalAns)
				System.out.println("Lower!");
			
	
		}
		
		System.out.println("Your ran out of guesses. The number was "+ finalAns);
		
		
	}

}