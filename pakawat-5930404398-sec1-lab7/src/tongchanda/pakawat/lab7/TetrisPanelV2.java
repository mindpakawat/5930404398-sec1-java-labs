package tongchanda.pakawat.lab7;




	import java.awt.Color;
	import java.awt.Graphics;
	import java.awt.Graphics2D;
	import java.util.Random;

	public class TetrisPanelV2 extends TetrisPanel implements Runnable {

	
		private static final long serialVersionUID = 1L;
		protected final static int RECT_WIDTH = 50;
		protected final static int SPEED = 2;
		protected int start = 0;
		protected int random = (int) (Math.random() * (WIDTH - RECT_WIDTH));
		

		Thread running;

		public TetrisPanelV2() {
			super();
			setBackground(Color.BLACK);
			running = new Thread(this);
			running.start();
		}

		@Override
		public void paint(Graphics g) {
			super.paintComponent(g);
			setPreferredSize(getPreferredSize());
			Graphics2D draw = (Graphics2D) g;
			draw.setColor(Color.YELLOW);
			draw.fillRect(random, start, RECT_WIDTH, RECT_WIDTH);
			
		}

		@Override
		public void run() {
			while (true) {
				dropping();
				repaint();
				
				try {
					Thread.sleep(25);
				} catch (InterruptedException ex) {

				}
			}
		}

		private void dropping() {
			
			start += SPEED;
			if (start >= HEIGHT) {
				start = 0;
				random = (int) (Math.random() * (WIDTH - RECT_WIDTH));
			}
		}

	}
