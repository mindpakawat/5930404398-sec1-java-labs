package tongchanda.pakawat.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class OutPatient extends Patient {

	public OutPatient(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);

	}

	private static String hospitalName;
	private LocalDate visitDate;
	public OutPatient(String name, String birthdate, Gender gender, double weight, int height, String visitDate) {
		super(name, birthdate, gender, weight, height);
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);
		
	}

	
		
	
	


	public LocalDate getVisitDate() {
		return visitDate;
	}


	public void setVisitDate(String visitDate) {
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);
	}

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
	
	public String toString(){
		return "OutPatient ["+ getName() +", birthdate = "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm. "+"visit date = "+visitDate+"]";
	}







	public static String getHospitalName() {
		return "Srinakarin";
	}







	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	
}
