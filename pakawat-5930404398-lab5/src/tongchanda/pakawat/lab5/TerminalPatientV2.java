package tongchanda.pakawat.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import tongchanda.pakawat.lab4.Gender;

public class TerminalPatientV2 extends PatientV3 implements HasInsurance , UnderLegalAge{
	
	private String terminalDisease ;
	private LocalDate firstDiagnosed;
	public TerminalPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}
	
	

	
	public String getTerminalDisease() {
		return terminalDisease;
	}
	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}
	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}
	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}


	
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	@Override
	public void askPermission() {
		
		
	}


	@Override
	public void askPermission(String legalGuardianName) {
		System.out.println("ask "+legalGuardianName+" for permission to treat him with chemo.");
		
	}


	@Override
	public void pay() {
		
		
	}


	@Override
	public void pay(double amount) {
		System.out.println("pay "+amount+" baht for his hospital visit by insurance.");

		
	}
	


}
