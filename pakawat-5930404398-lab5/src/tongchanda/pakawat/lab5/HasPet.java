package tongchanda.pakawat.lab5;

public interface HasPet {
	
	void feedPet();
	void playWithPet();

}
