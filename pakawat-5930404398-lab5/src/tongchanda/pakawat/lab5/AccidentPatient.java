package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.*;

public class AccidentPatient extends PatientV2{

	public AccidentPatient(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
		
	}

	private String typeOfAccident;
	private boolean isInICU;
	public AccidentPatient(String name, String birthdate, Gender gender, double weight, int height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}
	public String getTypeOfAccident() {
		return typeOfAccident;
	}
	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}
	public boolean isInICU() {
		return isInICU;
	}
	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
		
	}
	
	public void patientReport(){
		System.out.println("You are in accident");
	}
	@Override
	public String toString(){
		return "AccidentPatient [ "+typeOfAccident+ ", In ICU], Patient ["+ getName() +", "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm.]";
		
		
	}
	
	
}
