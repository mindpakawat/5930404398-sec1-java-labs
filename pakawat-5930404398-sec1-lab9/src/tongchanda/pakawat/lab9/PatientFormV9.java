package tongchanda.pakawat.lab9;

// this program have open file save file

//author Pakawat  Tongchanda  5930404398  sec 1

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PatientFormV9 extends PatientFormV8 {

	
	private static final long serialVersionUID = 1L;
	protected JFileChooser fileChoose = new JFileChooser();
	

	public PatientFormV9(String string) {
		super(string);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		PatientFormV9 patientFormV9 = new PatientFormV9 ("Patient Form V9");
		patientFormV9.setJMenuBar(patientFormV9.createMenus());
		patientFormV9.addComponents();
		patientFormV9.setFrameFeatures();
		patientFormV9.addListeners();
		patientFormV9.setAcceleratorKey();
		patientFormV9.setMnemonicKey();
		patientFormV9.actionPerform();
	}
	
	

	public void addComponents(){
		super.addComponents();
		
	}
	public void addListeners(){
		super.addListeners();
		openMenu.addActionListener(this);
		saveMenu.addActionListener(this);
		exitMenu.addActionListener(this);
		customColor.addActionListener(this);
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0){
		super.actionPerformed(arg0);
		
		if (arg0.getSource().equals(openMenu)) {

			int returnval = fileChoose.showOpenDialog(this);
			if (returnval == JFileChooser.APPROVE_OPTION) {

				java.io.File file = fileChoose.getSelectedFile();
				JOptionPane.showMessageDialog(fileChoose, "You have opened file " + file.getName());

			} else {
				JOptionPane.showMessageDialog(fileChoose, "Open command is cancelled");
			}

		}

		if (arg0.getSource().equals(saveMenu)) {
			int returnval = fileChoose.showSaveDialog(this);
			if (returnval == JFileChooser.APPROVE_OPTION) {

				java.io.File file = fileChoose.getSelectedFile();
				JOptionPane.showMessageDialog(fileChoose, "You have saved file " + file.getName());

			} else {
				JOptionPane.showMessageDialog(fileChoose, "Save command is cancelled");
			}
		}
		
		if (arg0.getSource().equals(exitMenu)) {
			dispose();
			System.exit(0);
		}
		
		if(arg0.getSource().equals(customColor))
		{
			Color newColor = JColorChooser.showDialog(this, "Select a Color", nameTxtField.getForeground());
			if (newColor != null) {
				nameTxtField.setForeground(newColor);
				dateTxtField.setForeground(newColor);
				weightTxtField.setForeground(newColor);
				heightTxtField.setForeground(newColor);
				addrTxtArea.setForeground(newColor);
				
				
			}
			
		}	
		
	}
	
}
