package tongchanda.pakawat.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import tongchanda.pakawat.lab4.Gender;

public class OlderPatient {
	
	public static void main(String[] args) {
		
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "Car accidrnt", true);
		PatientV2 weera = new TerminalPatient("weera","15.01.2000", Gender.MALE, 72, 172, "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.01.1999", Gender.MALE, 68, 170, "Fire accident", true);
	    duangjai.setName(((VIPPatient) duangjai).getVIPName("mickeymouse"));
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);
	}

	public static void isOlder(PatientV2 person1, PatientV2 person2){
		
		long oldYear1 = person1.getBirthdate().until(LocalDate.now(), ChronoUnit.YEARS); 
		long oldYear2 = person2.getBirthdate().until(LocalDate.now(), ChronoUnit.YEARS);
		if(oldYear1 >= oldYear2){
			System.out.println(person1.getName()+" is older than "+person2.getName());
		}
		else{
			System.out.println(person1.getName()+" is not older than "+person2.getName());
		}
			
	}
	
	public static void isOldest(PatientV2 person1, PatientV2 person2, PatientV2 person3){
		long oldYear1 = person1.getBirthdate().until(LocalDate.now(), ChronoUnit.YEARS);
		long oldYear2 = person2.getBirthdate().until(LocalDate.now(), ChronoUnit.YEARS);
		long oldYear3 = person3.getBirthdate().until(LocalDate.now(), ChronoUnit.YEARS);
		
	
		if(oldYear1>=oldYear2 && oldYear1>=oldYear3){
			
			System.out.println(person1.getName()+" is the oldest among "+person1.getName()+", "+person2.getName()+" and "+person3.getName());
		}
		else if (oldYear2>=oldYear1 && oldYear2>=oldYear3){
			
			System.out.println(person2.getName()+" is the oldest among "+person1.getName()+", "+person2.getName()+" and "+person3.getName());

		}
		else if(oldYear3>=oldYear1&&oldYear3>=oldYear2){
			
			System.out.println(person3.getName()+" is the oldest among "+person1.getName()+", "+person2.getName()+" and "+person3.getName());

		}
		
		
		
}
}
