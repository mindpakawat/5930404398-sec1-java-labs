package tongchanda.pakawat.lab9;

// this program add item for menu bar

//author Pakawat  Tongchanda  5930404398   sec 1

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.JMenuBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class PatientFormV8 extends PatientFormV7  {
	
	
	private static final long serialVersionUID = 1L;

	public PatientFormV8(String string) {
		super(string);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		PatientFormV8 patientFormV8 = new PatientFormV8 ("Patient Form V8");
		patientFormV8.setJMenuBar(patientFormV8.createMenus());
		patientFormV8.addComponents();
		patientFormV8.setFrameFeatures();
		patientFormV8.addListeners();
		patientFormV8.setAcceleratorKey();
		patientFormV8.setMnemonicKey();
	}

	public void addComponents(){
		super.addComponents();
		
	}
	protected void setMnemonicKey(){
		fileMenu.setMnemonic(KeyEvent.VK_F);
		newMenu.setMnemonic(KeyEvent.VK_N);
		openMenu.setMnemonic(KeyEvent.VK_O);
		saveMenu.setMnemonic(KeyEvent.VK_S);
		exitMenu.setMnemonic(KeyEvent.VK_X);
		
		configMenu.setMnemonic(KeyEvent.VK_C);
		
		colorMI.setMnemonic(KeyEvent.VK_L);
		blueColor.setMnemonic(KeyEvent.VK_B);
		greenColor.setMnemonic(KeyEvent.VK_G);
		redColor.setMnemonic(KeyEvent.VK_R);
		customColor.setMnemonic(KeyEvent.VK_U);
		
		sizeMI.setMnemonic(KeyEvent.VK_Z);
		sixteenSize.setMnemonic(KeyEvent.VK_6);
		twentySize.setMnemonic(KeyEvent.VK_0);
		twentyFourSize.setMnemonic(KeyEvent.VK_4);
		customSize.setMnemonic(KeyEvent.VK_M);
	}
	protected void setAcceleratorKey(){
		newMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.CTRL_MASK));
		openMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.CTRL_MASK));
		saveMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK));
		exitMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,ActionEvent.CTRL_MASK));
		
		blueColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.CTRL_MASK));
		greenColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,ActionEvent.CTRL_MASK));
		redColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,ActionEvent.CTRL_MASK));
		customColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,ActionEvent.CTRL_MASK));
		
		sixteenSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6,ActionEvent.CTRL_MASK));
		twentySize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0,ActionEvent.CTRL_MASK));
		twentyFourSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,ActionEvent.CTRL_MASK));
		customSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,ActionEvent.CTRL_MASK));
	
	
	
	}
	
	public void addListeners(){
		super.addListeners();
	}
	public JMenuBar createMenus(){
		super.createMenuBar();
		return menuBar;
		
	}

	
	
	
	
	
	
	
}
