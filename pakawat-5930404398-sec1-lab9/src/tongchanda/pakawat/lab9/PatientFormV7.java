package tongchanda.pakawat.lab9;

// this program have slider for blood pressure

//author Pakawat  tongchanda  5930404398  sec 1 

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tongchanda.pakawat.lab8.PatientFormV6;

public class PatientFormV7 extends PatientFormV6 implements ChangeListener{

	
	
	private static final long serialVersionUID = 1L;
	protected JSlider topSliders,bottomSliders;
	protected JPanel top,bottom,blood;
	protected JLabel topLabel,bottomLabel,bloodPreasure;
	
	public PatientFormV7(String string) {
		super(string);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		PatientFormV7 patientFormV7 = new PatientFormV7 ("Patient Form V7");
		patientFormV7.setJMenuBar(patientFormV7.createMenuBar());
		patientFormV7.addComponents();
		patientFormV7.setFrameFeatures();
		patientFormV7.addListeners();
	}
	
	public void addComponents(){
		super.addComponents();
		
		topLabel = new JLabel("Top number:");
		topSliders = new JSlider(0,200,100);
		topSliders.setMajorTickSpacing(50);
		topSliders.setMinorTickSpacing(10);
		topSliders.setPaintLabels(true);
		topSliders.setPaintTicks(true);
		topSliders.setLabelTable(topSliders.createStandardLabels(50));

		bottomLabel = new JLabel("Bottom number:");
		bottomSliders = new JSlider(0,200,100);
		bottomSliders.setMajorTickSpacing(50);
		bottomSliders.setMinorTickSpacing(10);
		bottomSliders.setPaintLabels(true);
		bottomSliders.setPaintTicks(true);
		bottomSliders.setLabelTable(bottomSliders.createStandardLabels(50));
		
		top = new JPanel();
		top.setLayout(new GridLayout(1, 2));
		top.add(topLabel);
		top.add(topSliders);
		
		bottom = new JPanel();
		bottom.setLayout(new GridLayout(1, 2));
		bottom.add(bottomLabel);
		bottom.add(bottomSliders);
		
		blood = new JPanel();
		blood.setLayout(new GridLayout(2, 2));
		blood.setBorder(BorderFactory.createTitledBorder("Blood Pressure"));
		blood.add(top,BorderLayout.CENTER);
		blood.add(bottom,BorderLayout.SOUTH);
		
		contentPanel.add(blood,BorderLayout.CENTER);
		overallPanel.add(contentPanel,BorderLayout.CENTER);
		
		
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0){
		
		Object src = arg0.getSource();
		
		if(src == okButton){
			JOptionPane.showMessageDialog(this , "Name = "+nameTxtField.getText()+" Birthdate = "+dateTxtField.getText()+" Weight = "+weightTxtField.getText()+" Height = "+heightTxtField.getText()+
					 "\nGender = "+ (maleRadioB.isSelected() ? "Male" : (femaleRadioB.isSelected() ? "Female": ""))+
					 "\nAddress = "+addrTxtArea.getText()+
					 "\nType = "+ (typeList.getSelectedItem())+"\nBlood Presure: Top Number = "+topSliders.getValue() +", Bottom Numnber = "+bottomSliders.getValue());
		}
		else if (src == cancelButton){
			cancleEmty();
		}
		else if (src == typeList){
			if(typeList.getSelectedItem() == "Outpatient")
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Outpatient");
			
		}
			if(typeList.getSelectedItem() == "Inpatient"){
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Inpatient");
		}
	}
	
	@Override
	public void stateChanged(ChangeEvent arg0) {
		JSlider src = (JSlider) arg0.getSource();
		if(!src.getValueIsAdjusting()){
			int value = src.getValue();
			JOptionPane.showMessageDialog(this, (src == topSliders ? "Top number" : "Bottom number")+" of blood presure is "+value);
			
		}
		
	}
	public void addListeners(){
		super.addListeners();
		topSliders.addChangeListener(this);
		bottomSliders.addChangeListener(this);
		
		
	}
	public JMenuBar createMenus(){
		super.createMenuBar();
		return menuBar;
		
	}
	
}
