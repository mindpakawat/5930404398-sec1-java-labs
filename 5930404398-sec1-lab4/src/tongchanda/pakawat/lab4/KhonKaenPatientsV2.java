package tongchanda.pakawat.lab4;

// This program is develop from KhonKaenPatients

// This program has added OutPatient

// Author Pakawat Tongchanda

// 06/02/1017

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class KhonKaenPatientsV2 {
	
	
	public static void main(String[] args){
		OutPatient chujai = new OutPatient("Chujai", "02.06.1995", Gender.FEMALE, 52.7, 150);
		OutPatient piti = new OutPatient("Piti", "13.08.1995", Gender.MALE, 52.7, 150, "21.01.2017");
		chujai.setVisitDate("25.01.2017");
		LocalDate dateBefore = piti.getVisitDate();
		LocalDate dateAfter = chujai.getVisitDate();
		long displayDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
		System.out.println(chujai);
		System.out.println(piti);
		System.out.println("Chujai visited after Piti for " + displayDaysBetween +" Days");
		System.out.println("Both of them went to "+ OutPatient.getHospitalName() + " hospital.");
	}

}
