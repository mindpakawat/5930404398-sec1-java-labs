package tongchanda.pakawat.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class TetrisPanel extends JPanel {

	protected Font font = new Font("Tetris", Font.BOLD, 30);

	private static final long serialVersionUID = 1L;
	final static int HEIGHT = 400;
	final static int WIDTH = 600;
	protected final static int BOARD_WIDTH = 30;
	protected final static int BOARD_HEIGHT = 20;
	static final int PIX_BLOCK = 20;

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);

	}

	public TetrisPanel() {
		super();
		setBackground(Color.LIGHT_GRAY);

	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		g2D.setPaint(Color.RED);
		g2D.setFont(font);
		int fontW = g2D.getFontMetrics().stringWidth("Tetris");
		g2D.drawString("Tetris", (WIDTH - fontW) / 2, HEIGHT / 4);

		int blockPosXZ = 2 * PIX_BLOCK;
		int blockPosYZ = 8 * PIX_BLOCK;
		TetrisShape zShape = new TetrisShape(Tetrimino.Z_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXZ + (zShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYZ + (zShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(zShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}

		int blockPosXS = 7 * PIX_BLOCK;
		int blockPosYS = 8 * PIX_BLOCK;
		TetrisShape sShape = new TetrisShape(Tetrimino.S_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXS + (sShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYS + (sShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(sShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}

		int blockPosXI = 11 * PIX_BLOCK;
		int blockPosYI = 8 * PIX_BLOCK;
		TetrisShape iShape = new TetrisShape(Tetrimino.I_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXI + (iShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYI + (iShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(iShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}
		int blockPosXT = 14 * PIX_BLOCK;
		int blockPosYT = 9 * PIX_BLOCK;
		TetrisShape tShape = new TetrisShape(Tetrimino.T_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXT + (tShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYT + (tShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(tShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}
		int blockPosXO = 19 * PIX_BLOCK;
		int blockPosYO = 9 * PIX_BLOCK;
		TetrisShape oShape = new TetrisShape(Tetrimino.O_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXO + (oShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYO + (oShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(oShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}
		int blockPosXL = 22 * PIX_BLOCK;
		int blockPosYL = 8 * PIX_BLOCK;
		TetrisShape lShape = new TetrisShape(Tetrimino.L_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXL + (lShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYL + (lShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(lShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}
		int blockPosXJ = 27 * PIX_BLOCK;
		int blockPosYJ = 8 * PIX_BLOCK;
		TetrisShape jShape = new TetrisShape(Tetrimino.J_SHAPE);
		for (int i = 0; i < 4; i++) {
			int posOfBlockX = blockPosXJ + (jShape.coordinateX[i] * PIX_BLOCK);
			int posOfBlockY = blockPosYJ + (jShape.coordinateY[i] * PIX_BLOCK);
			g2D.setColor(jShape.getColor());
			g2D.fillRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
			g2D.setColor(Color.BLACK);
			g2D.drawRect(posOfBlockX, posOfBlockY, PIX_BLOCK, PIX_BLOCK);
		}
	}
}