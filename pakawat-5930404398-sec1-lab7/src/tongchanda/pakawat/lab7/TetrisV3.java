package tongchanda.pakawat.lab7;

import javax.swing.SwingUtilities;

public class TetrisV3 extends TetrisV2{
	public TetrisV3(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		TetrisV3 ttV3 = new TetrisV3("Rectangles Dropping V2");
		ttV3.addComponents();
		ttV3.setFrameFeatures();
	}

	@Override
	public void addComponents() {
		TetrisPanelV3 tetrisPanelV3 = new TetrisPanelV3();
		add(tetrisPanelV3);
	}
}
