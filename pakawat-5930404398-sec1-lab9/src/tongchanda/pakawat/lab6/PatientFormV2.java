package tongchanda.pakawat.lab6;



import java.awt.*;
import javax.swing.*;

public class PatientFormV2 extends PatientFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3694909354550437224L;

	protected JRadioButton maleRadioB, femaleRadioB;
	protected ButtonGroup genderGrpB;
	protected JTextArea addrTxtArea;
	protected JScrollPane addrScrollPane;
	protected JPanel genderPanel, addrPanel, contentPanel, genderChoicesPanel, centerPanel;
	protected JLabel genderLabel, addrLabel;
	
	public final static int NUM_TXTAREA_ROWS = 2;
	public final static int NUM_TXTAREA_COLS = 35;
	
	public PatientFormV2(String title) {
		super(title);
	}
	
	protected void initComponents() {
		super.initComponents();
		maleRadioB = new JRadioButton("Male");
		femaleRadioB = new JRadioButton("Female");
		genderGrpB = new ButtonGroup();
		genderPanel = new JPanel(new GridLayout(1,2));
		genderChoicesPanel = new JPanel();
		addrTxtArea = new JTextArea(NUM_TXTAREA_ROWS, NUM_TXTAREA_COLS);
		addrTxtArea.setLineWrap(true);
		addrTxtArea.setWrapStyleWord(true);
		addrTxtArea.setText("Department of Computer Engineering, Faculty of Engineering, ");
		addrTxtArea.append("Khon Kaen University, Mittraparp Rd., T. Naimuang, ");
		addrTxtArea.append("A. Muang, Khon Kaen, Thailand, 40002");
		addrScrollPane = new JScrollPane(addrTxtArea);
		addrPanel = new JPanel(new BorderLayout());
		contentPanel = new JPanel(new BorderLayout());
		genderLabel = new JLabel("Gender:");
		addrLabel = new JLabel("Address:");
		centerPanel = new JPanel();
	}
	
	public void addComponents() {
		super.addComponents();
		genderChoicesPanel.add(maleRadioB);
		genderChoicesPanel.add(femaleRadioB);
		genderGrpB.add(maleRadioB);
		genderGrpB.add(femaleRadioB);
		genderPanel.add(genderLabel);
		genderPanel.add(genderChoicesPanel);
		addrPanel.add(addrLabel, BorderLayout.NORTH);
		addrPanel.add(addrScrollPane, BorderLayout.SOUTH);
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(textsPanel,BorderLayout.NORTH);
		centerPanel.add(genderPanel, BorderLayout.CENTER);
		contentPanel.add(centerPanel,BorderLayout.NORTH);
		contentPanel.add(addrPanel, BorderLayout.SOUTH);
		overallPanel.add(contentPanel, BorderLayout.CENTER);
	}
	public static void createAndShowGUI(){
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
