package tongchanda.pakawat.lab6;

// This program make simple window and have Ok button and Cancle button in simple window

// Author Pakawat Tongchanda

// 02/03/2017


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame{

	
	
	private static final long serialVersionUID = 1L;
	protected JButton okButton = new JButton();
	protected JButton cancleButton =  new JButton();
	protected JPanel panel = new JPanel();
	
	public MySimpleWindow(String string) {
		super(string);
	}
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });

	}
	private static void createAndShowGUI(){
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	protected void  addComponents(){
		
		//build the button
		okButton.setText("Ok");
		cancleButton.setText("Cancle");
		panel.setLayout(new FlowLayout());
		panel.add(cancleButton);
		panel.add(okButton);
		setContentPane(panel);
		
		
	}
	protected void setFrameFeatures(){
		//set simple widow in center of display
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
	}
}
