package tongchanda.pakawat.lab5;

import tongchanda.pakawat.lab4.Gender;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance,UnderLegalAge{

	
	private String typeOfAccident;
	private boolean isInICU;
	public AccidentPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}
	public String getTypeOfAccident() {
		return typeOfAccident;
	}
	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}
	public boolean isInICU() {
		return isInICU;
	}
	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
		
	}
	
	
	@Override
	public String toString(){
		return "AccidentPatient [ "+typeOfAccident+ ", In ICU], Patient ["+ getName() +", "+getBirthdate()+", "+getGender()+", "+getWeight()+" kg., "+getHeight()+" cm.]";
	}
	@Override
	public void pay() {
		System.out.println("pay the accident bill with insurance");
		
	}

	@Override
	public void pay(double amount) {
		
		
	}

	@Override
	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident.");
		
	}

	@Override
	public void askPermission(String legalGuardianName) {
		
		
	}

}
