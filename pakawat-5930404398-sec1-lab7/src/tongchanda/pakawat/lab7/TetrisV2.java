package tongchanda.pakawat.lab7;

import javax.swing.SwingUtilities;

public class TetrisV2 extends Tetris{

	
	private static final long serialVersionUID = 1L;

	public TetrisV2(String string) {
		super(string);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });

	}

	protected static void createAndShowGUI() {
		
		TetrisV2 ttV2 = new TetrisV2("Rectangle Droping");
		ttV2.addComponents();
		ttV2.setFrameFeatures();
	}

	public void addComponents() {
		TetrisPanelV2 tetrisPanelV2 = new TetrisPanelV2();
		add(tetrisPanelV2);
	}

}

