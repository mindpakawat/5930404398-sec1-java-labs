package tongchanda.pakawat.lab6;



import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;

public class PatientFormV1 extends MySimpleWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3629832942393810272L;
	protected JLabel nameLabel, dateLabel, weightLabel, heightLabel;
	protected JTextField nameTxtField, dateTxtField, weightTxtField, heightTxtField;
	protected JPanel namePanel, datePanel, weightPanel, heightPanel;
	protected JPanel overallPanel, textsPanel;
	public final static int TXTFIELD_WIDTH = 15;
	public final static int TOOLTIP_INIT_DELAY = 100;
	public final static int TOOLTIP_DISMISS_DELAY = 5000;
	
	public PatientFormV1(String title) {
		super(title);
		 ToolTipManager.sharedInstance().setInitialDelay(TOOLTIP_INIT_DELAY);
	     ToolTipManager.sharedInstance().setDismissDelay(TOOLTIP_DISMISS_DELAY);
	}
	
	protected void initComponents() {
		namePanel = new JPanel(new GridLayout(1,2));
		datePanel = new JPanel(new GridLayout(1,2));
		weightPanel = new JPanel(new GridLayout(1,2));
		heightPanel = new JPanel(new GridLayout(1,2));
		nameLabel = new JLabel("Name:");
		nameTxtField = new JTextField(TXTFIELD_WIDTH);
		dateLabel = new JLabel("Birthdate:");
		dateTxtField = new JTextField(TXTFIELD_WIDTH);
		dateTxtField.setToolTipText("ex. 22.02.2000");
		weightLabel = new JLabel("Weight (kg.):");
		weightTxtField = new JTextField(TXTFIELD_WIDTH);
		heightLabel = new JLabel("Height (metre):");
		heightTxtField = new JTextField(TXTFIELD_WIDTH);
		textsPanel = new JPanel(new GridLayout(4,1));
		overallPanel = new JPanel(new BorderLayout());
	}
	protected void setLabelTxtField(JPanel panel,
			JLabel label, JTextField txtField) {
		panel.add(label);
		panel.add(txtField);
	}
	public void addComponents() {
		super.addComponents();
		initComponents();
		setLabelTxtField(namePanel, nameLabel, nameTxtField);
		setLabelTxtField(datePanel, dateLabel, dateTxtField);
		setLabelTxtField(weightPanel, weightLabel, weightTxtField);
		setLabelTxtField(heightPanel, heightLabel, heightTxtField);
		textsPanel.add(namePanel);
		textsPanel.add(datePanel);
		textsPanel.add(weightPanel);
		textsPanel.add(heightPanel);
		overallPanel.add(textsPanel, BorderLayout.NORTH);
		overallPanel.add(buttonsPanel, BorderLayout.SOUTH);
		setContentPane(overallPanel);
	}
	
	public static void createAndShowGUI(){
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}