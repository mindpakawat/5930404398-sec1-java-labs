package tongchanda.pakawat.lab6;

// This program is develop from MySimpleWindow . This add text and text field

// Author Pakawat Tongchanda

// 02/03/2017

import java.awt.*;
import javax.swing.*;

public class PatientFormV1 extends MySimpleWindow {

	final static int textNum = 15;

	private static final long serialVersionUID = 1L;
	protected JLabel name = new JLabel();
	protected JLabel birtdate = new JLabel();
	protected JLabel weight = new JLabel();
	protected JLabel height = new JLabel();
	protected JTextField nameText = new JTextField();
	protected JTextField birtdateText = new JTextField();
	protected JTextField weightText = new JTextField();
	protected JTextField heightText = new JTextField();
	protected JPanel intext = new JPanel();
	protected JPanel inlabel = new JPanel();
	protected JPanel areaPanel = new JPanel();
	protected JPanel display = new JPanel();

	public PatientFormV1(String string) {
		super(string);

	}

	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		//Use super class and set the name of label text
		super.addComponents();
		name = new JLabel("Name:");
		birtdate = new JLabel("Birtdate:");
		weight = new JLabel("Weight (kg.):");
		height = new JLabel("Height (matre):");
		nameText = new JTextField(textNum);
		birtdateText = new JTextField(textNum);
		weightText = new JTextField(textNum);
		heightText = new JTextField(textNum);
		birtdateText.setToolTipText("ex. 22.02.2000");
		intext.setLayout(new GridLayout(0, 1));
		inlabel.setLayout(new GridLayout(0, 1));
		inlabel.add(name);
		inlabel.add(birtdate);
		inlabel.add(weight);
		inlabel.add(height);
		intext.add(nameText);
		intext.add(birtdateText);
		intext.add(weightText);
		intext.add(heightText);
		
		//Output all text and text field
		display.setLayout(new BorderLayout(8,2));
		areaPanel.setLayout(new BorderLayout(10,2));
		areaPanel.add(inlabel, BorderLayout.WEST);
		areaPanel.add(intext, BorderLayout.EAST);
		display.add(areaPanel, BorderLayout.NORTH);
		display.add(panel, BorderLayout.SOUTH);
		setContentPane(display);
	}

}
