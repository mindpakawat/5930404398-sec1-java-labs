package tongchanda.pakawat.lab3;

import java.util.Scanner;

public class GuessNumberMethodGame {

	static int remainingGuess = 7;
	static int max = 100, min = 0;
	static int answer;

	public static void main(String[] args) {
		genAnswer();
		playGame();

	}

	public static void genAnswer() {
		answer = min + (int) (Math.random() * ((max - min) + 1));
	}

	public static void playGame() {
		Scanner ScGuess = new Scanner(System.in);

		for (int i = remainingGuess; i > 0; i--) {
			System.out.println("Number of remaining is: " + i);
			System.out.print("Enter a guess: ");
			int guess = ScGuess.nextInt();

			if (guess == answer) {
				System.out.println("Correct!");
				return;
			}

			else if (guess < answer)
				System.out.println("Higher!");

			else if (guess > answer)
				System.out.println("Lower!");

		}

		System.out.println("Your ran out of guesses. The number was " + answer);

	}
}